import sys
from PyQt6.QtWidgets import (
    QApplication,
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QPushButton,
    QLabel,
    QLineEdit,
    QTableWidget,
    QTableWidgetItem,
    QHeaderView
)
from pymongo import MongoClient

class ProductInventoryApp(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Product Inventory')
        self.product_inventory = []
        self.mongo_client = MongoClient('mongodb://localhost:27017/')  
        self.db = self.mongo_client['product_inventory']
        self.collection = self.db['products']
        
        main_layout = QVBoxLayout()
        product_name_label = QLabel('Product Name:')
        self.product_name_input = QLineEdit()
        product_quantity_label = QLabel('Quantity:')
        self.product_quantity_input = QLineEdit()
        product_units_label = QLabel('Units:')
        self.product_units_input = QLineEdit()
        product_currency_label = QLabel('Currency:')
        self.product_currency_input = QLineEdit()
        product_price_label = QLabel('Price:')
        self.product_price_input = QLineEdit()
        input_layout = QVBoxLayout()
        input_layout.addWidget(product_name_label)
        input_layout.addWidget(self.product_name_input)
        input_layout.addWidget(product_quantity_label)
        input_layout.addWidget(self.product_quantity_input)
        input_layout.addWidget(product_units_label)
        input_layout.addWidget(self.product_units_input)
        input_layout.addWidget(product_price_label)
        input_layout.addWidget(self.product_price_input)
        input_layout.addWidget(product_currency_label)
        input_layout.addWidget(self.product_currency_input)
        button_layout = QHBoxLayout()
        add_button = QPushButton('Add Product')
        add_button.clicked.connect(self.add_product)
        remove_button = QPushButton('Remove Product')
        remove_button.clicked.connect(self.remove_product)
        button_layout.addWidget(add_button)
        button_layout.addWidget(remove_button)
        self.product_table = QTableWidget()
        self.product_table.setColumnCount(5)
        self.product_table.setHorizontalHeaderLabels(['Product Name', 'Quantity', 'Units', 'Price', 'Currency'])
        self.product_table.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)
        main_layout.addLayout(input_layout)
        main_layout.addLayout(button_layout)
        main_layout.addWidget(self.product_table)
        self.setLayout(main_layout)

    def add_product(self):
        product_name = str(self.product_name_input.text())
        quantity = int(self.product_quantity_input.text())
        unit = str(self.product_units_input.text())
        price = float(self.product_price_input.text())
        currency = str(self.product_currency_input.text())

        if product_name and quantity and unit and price and currency:
            product_data = {'name': product_name, 'quantity': quantity, 'units': unit, 'price': price, 'currency': currency}
            self.collection.insert_one(product_data)  # Insert data into MongoDB
            self.update_table()
            self.product_name_input.clear()
            self.product_quantity_input.clear()
            self.product_currency_input.clear()
            self.product_price_input.clear()
            self.product_units_input.clear()

    def remove_product(self):
        selected_row = self.product_table.currentRow()
        if selected_row != -1:
            product_name = self.product_table.item(selected_row, 0).text()
            self.collection.delete_one({'name': product_name}) 
            self.update_table()

    def update_table(self):
        self.product_table.setRowCount(0)
        products = self.collection.find()
        for product in products:
            row_position = self.product_table.rowCount()
            self.product_table.insertRow(row_position)
            self.product_table.setItem(row_position, 0, QTableWidgetItem(product['name']))
            self.product_table.setItem(row_position, 1, QTableWidgetItem(str(product['quantity'])))
            self.product_table.setItem(row_position, 2, QTableWidgetItem(product['units']))
            self.product_table.setItem(row_position, 3, QTableWidgetItem(str(product['price'])))
            self.product_table.setItem(row_position, 4, QTableWidgetItem(product['currency']))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = ProductInventoryApp()
    window.show()
    sys.exit(app.exec())
